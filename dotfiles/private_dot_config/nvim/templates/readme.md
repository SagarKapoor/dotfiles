# Project Name

<!-- Add some README badges over here. -->

A brief description of the project written in not more than a paragraph or two.

## How to Get Started With the Project

Detail some necessary steps & procedure to get started with using/working on the
project.

## Contributing to the Project

Detail the necessary steps & guidelines on developing or contributing to the
project.

## Distribution & Licensing Rights

The project is licensed under the terms & conditions (T&Cs) of the MIT license.
So feel free to use, distribute, copy, modify & such with the content of this
repository in whatever manner you want to. The caveat is, your usage should
adhere to the T&Cs of the MIT license.

For more information on the licensing T&Cs, feel free to refer to the
[LICENSE](./LICENSE) document.
