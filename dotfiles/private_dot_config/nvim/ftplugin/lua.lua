-- Add a coloured column on the right side to showcase the recommended length of a single line of code.
vim.opt.colorcolumn = { 120 }
